# Arduino VGA sync

Arduino
([ATmega328P](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf))
code to generate sync pulses for a VGA display.

Resolution: 640x480 @ 60Hz

In addition to `Vsync`/`Hsync` it also generates `Vblank` and `Hblank` pulses,
which are high during the blanking periods.


## Pin outputs

| Pin   | Signal |
|-------|--------|
| A4    | Hsync  |
| A5    | Vsync  |
| A2    | Hblank |
| A3    | Vblank |

## Compiling/Uploading (`arduino-cli`)

This is just a note for myself so I don't waste 5 minutes looking for the
right parameters for `arduino-cli` the next time I look at this. If you have
the Arduino IDE you can just use that.

	arduino-cli compile -u -p /dev/ttyUSB0 -b arduino:avr:nano --board-options cpu=atmega328old
