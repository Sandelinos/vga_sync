#define LINE_CYCLES  508
#define FRAME_LINES  525

volatile unsigned int linecount;
volatile byte hsync_byte = 0b00111100;

extern "C" {
	void hbi(byte);
}

void setup() {
	pinMode(A2, OUTPUT); // Hblank
	pinMode(A3, OUTPUT); // Vblank
	pinMode(A4, OUTPUT); // Hsync
	pinMode(A5, OUTPUT); // Vsync

	TIMSK0 = 0; // Disable TIMER0 interrupts


	TCCR1A  = 0;
	TCCR1B  = _BV(WGM12);  // Set Waveform Generation Mode to 4
	TCCR1B |= _BV(CS10);   // Set TIMER1 prescaler to /1
	OCR1A   = LINE_CYCLES; // Make TIMER1 count to LINE_CYCLES
	TIMSK1  = _BV(OCIE1A); // Interrupt when timer1 overflows
}

ISR(TIMER1_COMPA_vect) {
	hbi(hsync_byte); // handle horizontal blanking interval in assembly
	// HBI is done, now we have time to do some logic during the visible line
	//                                     VHVH
	//                                     SSBB
	if (linecount == 0)   hsync_byte = 0b00011000;// Vsync low, Vblank high (V sync)
	if (linecount == 2)   hsync_byte = 0b00111000;// Vsync high,Vblank high (V backporch)
	if (linecount == 35)  hsync_byte = 0b00110000;// Vsync high,Vblank low  (visible)
	if (linecount == 515) hsync_byte = 0b00111000;// Vsync high,Vblank high (V frontporch)
	if (++linecount == FRAME_LINES) linecount = 0;
}

void loop() {}
